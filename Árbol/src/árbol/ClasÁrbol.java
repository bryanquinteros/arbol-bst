
package árbol;


public class ClasÁrbol {
    
    Nodo Raiz;
    int tam;
    
    public ClasÁrbol(){
    
        Raiz = null;
        tam = 0;
        
    
    }
    
    public void Insertar(int Dato){
    
    
    if(Raiz == null)
        
        Raiz = new Nodo(Dato);
    
    else{
          Nodo aux = Raiz;
      
          if(Dato<aux.Dato){ 
              
           if(aux.Izq!=null)
              aux = aux.Izq;
           else
              aux.Izq = new Nodo(Dato,aux);
             
          }
      
          else{
               if(aux.Der!=null)
              aux = aux.Der;
                else
              aux.Der = new Nodo(Dato,aux);
          }
     }
    
    }
    
    public boolean Buscar(int Dato){
    
    Nodo aux = Raiz;
    
    while(aux!=null){
    
        if(aux.Dato == Dato)
            
            return true;
        if(Dato<aux.Dato)
            aux = aux.Izq;
        
        else
            aux = aux.Der;
      
       }
  
    return false;
    
    }
    
    
    
    
    
    
    //InOrden
    private String inOrden(Nodo n){
    
        if(n!=null)
        {
            
        return inOrden(n.Izq) + n.Dato + " , " + inOrden(n.Der);
          
        }
        return "";
    }
    
    public String inOrden(){
        
    return inOrden(Raiz);
    
    }
    
    //PreOrden 
     private String prOrden(Nodo n){
    
        if(n!=null)
        {
           
        return  n.Dato + "," + prOrden(n.Izq) + prOrden(n.Der);
          
        }
        
        return"";
        
    }
    
    public String prOrden(){
        
    return prOrden(Raiz);
    
    }
     
    //Posorden
    
     private String psOrden(Nodo n){
    
        if(n!=null)
        {
          return psOrden(n.Izq) + psOrden(n.Der) +  n.Dato + ","; 
          
        }
        return"";
    }
    
    public String psOrden(){
        
    return psOrden(Raiz);
    
    }
    
    
    
}
