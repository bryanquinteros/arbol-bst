
package árbol;


public class Nodo {
    
    int Dato;
    
    Nodo Izq;
    Nodo Der;
    Nodo Pad;
    
    
    public Nodo(int Dato){
    this.Dato=Dato;
    Izq=null;
    Der=null;
    Pad=null;
    
    }
    public Nodo(int Dato, Nodo Pad){
        
    
    this.Dato=Dato;
    Izq=null;
    Der=null;
    this.Pad=Pad;
    
    }
    
    
}
